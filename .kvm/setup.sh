#!/bin/bash

sudo pacman -S qemu-base dmidecode libvirt edk2-ovmf virt-manager dnsmasq ebtables qemu-hw-usb-host

sudo systemctl enable --now virtlogd.socket 
sudo systemctl enable --now libvirtd.service 
sudo virsh net-autostart default 
sudo virsh net-start default

sudo ln -sf $HOME/.desktop/.kvm/hooks /etc/libvirt/

echo "done"
