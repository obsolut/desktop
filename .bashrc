#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

alias copy="xclip -selection clipboard"

neofetch

checksha () {
	if [ $(sha256sum $1 | cut -d " " -f 1) == $2 ]; then
		echo "Valid!"
	else
		echo "Invalid!"
	fi
}
