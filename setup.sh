#!/bin/bash

echo "password" | sudo -kS pacman -S archlinux-keyring --noconfirm

echo "password" | sudo -kS pacman -S bspwm sxhkd kitty feh lightdm lightdm-gtk-greeter firefox ranger rofi xorg-server xorg-xinit ttf-hack ttf-font-awesome lxsession-gtk3 papirus-icon-theme python-pywal pacman-contrib pulseaudio pulsemixer deluge-gtk xorg-xset xorg-xsetroot python3 xclip neofetch polybar amd-ucode nvidia python-pillow gnome-themes-extra --noconfirm

mkdir -pv $HOME/.cache $HOME/.config/gtk-3.0/ $HOME/downloads $HOME/isos $HOME/screenshots $HOME/images $HOME/code

ln -sfv $HOME/.desktop/.config/bspwm $HOME/.config/
ln -sfv $HOME/.desktop/.config/mimeapps.list $HOME/.config/
ln -sfv $HOME/.desktop/.config/sxhkd $HOME/.config/
ln -sfv $HOME/.desktop/.config/kitty $HOME/.config/
ln -sfv $HOME/.desktop/.config/ranger $HOME/.config/
ln -sfv $HOME/.desktop/.config/neofetch $HOME/.config/
ln -sfv $HOME/.desktop/.config/polybar $HOME/.config/
ln -sfv $HOME/.desktop/.config/rofi $HOME/.config/
ln -sfv $HOME/.desktop/.config/gtk-3.0/settings.ini $HOME/.config/gtk-3.0/
ln -sfv $HOME/.desktop/.config/gtk-3.0/bookmarks $HOME/.config/gtk-3.0/
ln -sfv $HOME/.desktop/.wallpaper $HOME/
ln -sfv $HOME/.desktop/.scripts $HOME/
ln -sfv $HOME/.desktop/.gitconfig $HOME/
ln -sfv $HOME/.desktop/.bashrc $HOME/
ln -sfv $HOME/.desktop/.nvidia-settings-rc $HOME/
ln -sfv $HOME/.desktop/.xprofile $HOME/
ln -sfv $HOME/.desktop/.Xresources $HOME/

cp $HOME/.desktop/.cache/rofi3.druncache $HOME/.cache/

echo "password" | sudo -kS mkdir /etc/pacman.d/hooks

echo "password" | sudo -kS cp $HOME/.desktop/.nvidia/nvidia.hook /etc/pacman.d/hooks/
echo "password" | sudo -kS cp $HOME/.desktop/.nvidia/70-nvidia.rules /etc/udev/rules.d/
echo "password" | sudo -kS cp $HOME/.desktop/.nvidia/xorg.conf /etc/X11/

echo "password" | sudo -kS systemctl enable lightdm

wal -i $HOME/.wallpaper/wallpaper.png

sleep 10

poweroff
